# -*- coding: utf-8 -*-
{
    'name': 'Filter products by supplier in purchase order',
    'version': '1.0',
    'category': 'Purchase',
    'author':  'Jamotion GmbH',
    'website': 'https://jamotion.ch',
    'summary': 'Product list filtered by seller',
    'images': ['static/description/screenshot.png'],
    'depends': [
        'purchase',
    ],
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/ir_sample_view',
    ],
    'demo': [],
    'test': [],
    'application': False,
    'active': False,
    'installable': True,
}
